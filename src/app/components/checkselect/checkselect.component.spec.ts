import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckselectComponent } from './checkselect.component';

describe('CheckselectComponent', () => {
  let component: CheckselectComponent;
  let fixture: ComponentFixture<CheckselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
